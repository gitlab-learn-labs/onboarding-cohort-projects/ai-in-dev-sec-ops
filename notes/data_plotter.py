import matplotlib.pyplot as plt
import numpy as np

# Sample data
x = np.arange(0, 10) 
y = x ** 2

# Simple line plot
plt.plot(x, y)
plt.xlabel('X')
plt.ylabel('Y')
plt.title('Simple Plot')

# Saving figure
plt.savefig('my_plot.png') 

# Showing figure
plt.show() 

print('Plot saved to my_plot.png')